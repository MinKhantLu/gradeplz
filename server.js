
const express = require('express');
// const ejs = require('ejs');
const MongoClient = require('mongodb').MongoClient;
const bodyParser = require('body-parser');
const app = express();
const db = require('./config/db');
const port = 8000;

app.use(bodyParser.urlencoded({ extended: true }));
app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
});

MongoClient.connect(db.url, (err, db) => {
  if (err) return console.log(err)
  require('./app/routes')(app, db);
  app.listen(port, () => {
    console.log('We are live ons ' + port);
  });
});
