var ObjectID = require('mongodb').ObjectID;
module.exports=function(app,db){
  app.get('/subjects/all', (req, res) => {
      db.collection('subjects').find({}).toArray((err,item)=>{
        if(err) res.send({'error':'An error has occured'});
        else  res.send({'data':item});
      });
  });
  app.get('/subjects/searchyear/:year', (req,res) =>{
      var year = req.params.year;
      var query = [{$match: {'year': year}}];
      db.collection('subjects').aggregate(query).toArray((err, item)=>{
        if (err) {
            res.send(err);
        }
        else {
            res.send(item);
        }
      });

  });
}
