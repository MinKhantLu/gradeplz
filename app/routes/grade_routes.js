var ObjectID = require('mongodb').ObjectID;
var dbDataCount = 0 ;
var pageLimit = 20;
var lastObjID = 0;
var query = "";

module.exports = function(app, db) {

  app.get('/grades/show?',(req,res) =>{
    db.collection('grades').count({},function(error,count){
        dbDataCount = count;
        query = [{$match: {'year': req.query.year}}];
        if(req.query.page>Math.ceil(dbDataCount/pageLimit)){
          res.send('Page count is oversized');
        }else{
          if(req.query.page>1){
            query = [{$match: {'year': req.query.year,'_id':{'$gt':lastObjID}}}];
            db.collection('grades').aggregate(query).limit(20).toArray((err,item)=>{
              lastObjID = ObjectID(item[item.length-1]._id);
              res.send(item);
            })
          }else{
            db.collection('grades').aggregate(query).limit(20).toArray((err,item)=>{
              lastObjID = ObjectID(item[item.length-1]._id);
              res.send(item);
            })
          }
        }
        });
    });

  app.get('/grades/all', (req, res) => {
    db.collection('grades').find({}).toArray((err,item)=>{
      if(err) res.send({'error':'An error has occured'});
      else  res.send({'data':item}); 
    });
  });

	app.put('/grades/update/:rollNo', (req, res) => {
    	var updateRollno = { rollNo : req.params.rollNo};
    	var updateDetails = {
        stuName : req.body.stuName,
        rollNo : req.body.rollNo,
        subj1 : req.body.subj1,
        subj2 : req.body.subj2,
        subj3 : req.body.subj3,
        subj4 : req.body.subj4,
        subj5 : req.body.subj5,
        subj6 : req.body.subj6,
        subj7 : req.body.subj7,
        srn : req.body.srn,
        academic_year : req.body.academic_year,
        term : req.body.term,
        major : req.body.major,
				year : req.body.year
      };
    	db.collection('grades').update(updateRollno, updateDetails, (err, result) => {
      	if (err) {
          res.send({'error':'An error has occurred'});
      	} else {
          res.send("Updated");
      	}
    	});
  	});

  app.get('/grades/searchrollno/:rollNo', (req,res) =>{
      var rollNo = req.params.rollNo;
      query = [{$match: {'rollNo': rollNo}}];
      db.collection('grades').aggregate(query).toArray((err, item)=>{
        if (err) {
            res.send(err);
        }
        else {
            res.send(item);
        }
      });

  });

  app.get('/grades/searchname/:stuName', (req,res) =>{
      var stuName = req.params.stuName;
      query = [{$match: {'stuName': stuName}}];
      db.collection('grades').aggregate(query).toArray((err, item)=>{
        if (err) {
            res.send(err);
        }
        else {
            res.send(item);
        }
      });

  });
	app.get('/grades/searchyear/:year', (req,res) =>{
			var year = req.params.year;
			query = [{$match: {'year': year}}];
			db.collection('grades').aggregate(query).toArray((err, item)=>{
				if (err) {
						res.send(err);
				}
				else {
						res.send(item);
				}
			});

	});

  app.get('/grades/searchsrn/:srn',(req,res)=>{
    query = [{$match: { 'srn' : req.params.srn}}];
    db.collection('grades').aggregate(query).toArray((err,item)=>{
      if(err){
        res.send(err);
      }else{
        res.send({'data': item});
      }
    })
  });
  app.get('/grades/search/:academic_year',(req,res)=>{
    var yearArray = ['First Year','Second Year','Third Year','Fourth Year','Fifth Year']
    var data = {};
    query = [{$match: { 'year' : yearArray[0], 'academic_year' : req.params.academic_year}}]
    db.collection('grades').aggregate(query).toArray((err,item)=>{
      data['First Year'] = item;
    })

    query = [{$match: { 'year' : yearArray[1], 'academic_year' : req.params.academic_year}}]
    db.collection('grades').aggregate(query).toArray((err,item)=>{
      data['Second Year'] = item;
    })

    query = [{$match: { 'year' : yearArray[2], 'academic_year' : req.params.academic_year}}]
    db.collection('grades').aggregate(query).toArray((err,item)=>{
      data['Third Year'] = item;
    })

    query = [{$match: { 'year' : yearArray[3], 'academic_year' : req.params.academic_year}}]
    db.collection('grades').aggregate(query).toArray((err,item)=>{
      data['Fourth Year'] = item;
    })

    query = [{$match: { 'year' : yearArray[4], 'academic_year' : req.params.academic_year}}]
    db.collection('grades').aggregate(query).toArray((err,item)=>{
      data['Fifth Year'] = item;
      res.send(data);
    })
  });

  app.get('/grades/search/:year/:academic_year',(req,res)=>{
    query = [{$match : {'year' : req.params.year , 'academic_year' : req.params.academic_year , 'term' : 'Second Term'}}];
    db.collection('grades').aggregate(query).toArray((err,item)=>{
      res.send({'data' : item});
    })
  });

  app.get('/grades/search/:major/:year/:academic_year',(req,res)=>{
    query = [{$match : {'major' : req.params.major , 'year' : req.params.year , 'academic_year' : req.params.academic_year}}];
    db.collection('grades').aggregate(query).toArray((err,item)=>{
      res.send({'data' : item});
    })
  });

  app.get('/grades/academic_year/all',(req,res)=>{
    db.collection('academic_year').find({},{'_id':0,'aca_year':1}).toArray((err,item)=>{
      res.send({'data':item});
    })
  });
  
  app.delete('/grades/delete/:rollNo', (req, res) => {
      var delRollNo = req.params.rollNo;
      var details = { rollNo : delRollNo };
      db.collection('grades').deleteOne(details, (err, item) => {
        if (err) {
          res.send({'error':'An error has occurred'});
        } else {
          res.send('RollNo ' + delRollNo + ' deleted!');
        }
    })
  });

  app.post('/grades/create', (req, res) => {
    var createStuInfo = {
        stuName : req.body.stuName,
        rollNo : req.body.rollNo,
        subj1 : req.body.subj1,
        subj2 : req.body.subj2,
        subj3 : req.body.subj3,
        subj4 : req.body.subj4,
        subj5 : req.body.subj5,
        subj6 : req.body.subj6,
        subj7 : req.body.subj7,
        srn : req.body.srn,
        year : req.body.year,
        academic_year : req.body.academic_year,
        term : req.body.term,
        major : req.body.major,
				year : req.body.year
      };
    db.collection('grades').insert(createStuInfo, (err, result) => {
      if (err) {
        res.send({ 'error':  err });
      } else {
        res.send(result.ops[0]);
      }
    });
  });
};
