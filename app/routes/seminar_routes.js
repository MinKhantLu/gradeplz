var ObjectID = require('mongodb').ObjectID;
var year = "";
module.exports = function(app, db) {
	
	app.get('/seminars/all', (req, res) => {
    	db.collection('seminars').find({}).toArray((err,item)=>{
      	if(err) res.send({'error':'An error has occured'});
      	else  res.send({'data':item}); 
    	});
  	});
  	app.get('/seminars?',(req,res)=>{
  		switch(req.query.year){
  			
      	case '1': year = "First Year";break;
      	case '2': year = "Second Year";break;
      	case '3': year = "Third Year";break;
      	case '4': year = "Fourth Year";break;
      	case '5': year = "Fifth Year";break;
    	}
  		var query = [{$match : {'year' : year}}];
  		db.collection('seminars').aggregate(query).toArray((err,item)=>{
  			if(err) res.send({'error' : 'An error has occured'});
  			else res.send(item);
  		});
  	});
  	
}