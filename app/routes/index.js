const gradeRoutes = require('./grade_routes');
const subjectRoutes = require('./subject_routes');
const assignmentRoutes = require('./assignment_routes');
const tutorialRoutes = require('./tutorial_routes');
const seminarRoutes = require('./seminar_routes');
const activityRoutes = require('./other_activities_routes');
module.exports = function(app, db) {
  gradeRoutes(app, db);
  subjectRoutes(app, db);
  assignmentRoutes(app, db);
  tutorialRoutes(app, db);
  seminarRoutes(app, db);
  activityRoutes(app, db);
  // Other route groups could go here, in the future
};
