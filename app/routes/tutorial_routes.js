var ObjectID = require('mongodb').ObjectID;
var year = "";
module.exports = function(app, db) {

	app.get('/tutorials/all', (req, res) => {
    db.collection('tutorials').find({}).toArray((err,item)=>{
      if(err) res.send({'error':'An error has occured'});
      else  res.send({'data':item}); 
      console.log(item);
    });
  });

  app.get('/tutorials?',(req,res) =>{
    switch(req.query.year){
      case '1': year = "First Year";break;
      case '2': year = "Second Year";break;
      case '3': year = "Third Year";break;
      case '4': year = "Fourth Year";break;
      case '5': year = "Fifth Year";break;
    }
    console.log(year);
    var query = [{$match: {'major': req.query.major , 'year': year}}];
    db.collection('tutorials').aggregate(query).toArray((err,item)=>{
          res.send(item);
    });
  });

	app.get('/tutorials/byYear/:year', (req,res) =>{
      var year = req.params.year;
      query = [{$match: {'year': year}}];
      db.collection('tutorials').aggregate(query).toArray((err, item)=>{
        if (err) {
            res.send(err);
        }
        else {
            res.send(item);
        }
      });

  });
	app.get('/tutorials/byMajor/:major',(req,res)=>{
		var major = req.params.major;
      	query = [{$match: {'major': major}}];
      	db.collection('tutorials').aggregate(query).toArray((err, item)=>{
        	if (err) {
            	res.send(err);
        	}
        	else {
            	res.send(item);
        	}
      	});
	});
};