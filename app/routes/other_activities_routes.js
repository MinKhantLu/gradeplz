var ObjectID = require('mongodb').ObjectID;
module.exports = function(app, db) {
	
	app.get('/activities/all', (req, res) => {
    db.collection('other_activities').find({}).toArray((err,item)=>{
      if(err) res.send({'error':'An error has occured'});
      else  res.send(item); 
    });
  });
}